#!/usr/bin/env python3

import os
import csv
from pelican.readers import MarkdownReader
from pelican.settings import read_settings


def update_md_file(md_filename, parsed_ids):
    output_lines = []
    with open(md_filename, 'r') as fp:
        is_parsing_ids = False
        for line in fp.readlines():
            if is_parsing_ids and len(line.strip()) > 0:
                output_lines.append(line)
                object_id = line.split('|')[1].strip()
                if object_id in parsed_ids:
                    del parsed_ids[object_id]
            if not is_parsing_ids:
                output_lines.append(line)
                if line.strip() == '|---|---|':
                    is_parsing_ids = True
    for object_id, object_name in parsed_ids.items():
        output_lines.append('| {object_id} | {object_name} |\n'.format(object_id=object_id, object_name=object_name))
    with open(md_filename, 'w') as fp:
        fp.writelines(output_lines)


def update_people_docs():
    pelican_settings = read_settings('../pelicanconf.py')
    pelican_md_reader = MarkdownReader(settings=pelican_settings)
    parsed_people_ids = {}
    for person_md_file in os.listdir('../content/people/'):
        _, person_md_info = pelican_md_reader.read(os.path.join('../content/people/', person_md_file))
        if person_md_info['active'].lower().strip() != 'yes':
            continue
        parsed_people_ids[person_md_info['id']] = ' '.join([person_md_info['name'], person_md_info['surname']])
    with open(os.path.join('..', pelican_settings['PATH_TO_MISSING_ID_TO_PEOPLE_INFO']), 'r') as fp:
        reader = csv.DictReader(fp)
        for row in reader:
            parsed_people_ids[row['id']] = row['name_surname']
    update_md_file('../docs/people_list.md', parsed_people_ids)


def update_projects_docs():
    pelican_settings = read_settings('../pelicanconf.py')
    pelican_md_reader = MarkdownReader(settings=pelican_settings)
    parsed_projects_ids = {}
    for project_md_file in os.listdir('../content/projects/'):
        _, project_md_file = pelican_md_reader.read(os.path.join('../content/projects/', project_md_file))
        if project_md_file['active'].lower().strip() != 'yes':
            continue
        parsed_projects_ids[project_md_file['id']] = project_md_file['title']
    with open(os.path.join('..', pelican_settings['PATH_TO_MISSING_ID_TO_PROJECTS_INFO']), 'r') as fp:
        reader = csv.DictReader(fp)
        for row in reader:
            parsed_projects_ids[row['id']] = row['title']
    update_md_file('../docs/projects_list.md', parsed_projects_ids)


if __name__ == '__main__':
    update_people_docs()
    update_projects_docs()
