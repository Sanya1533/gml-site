Active: yes
Category: people
PersonType: staff
Id: vlad_shakhuro
Name: Vlad
Surname: Shakhuro
PublicationsPossibleNames: Влад, Vladislav, Владислав
PublicationsPossibleSurnames: Шахуро
ParseNewPublications: yes
LabGroups: cv_group
PersonOrder: 20
Position: Junior researcher
Email: vlad.shakhuro@graphics.cs.msu.ru
IstinaPage: https://istina.msu.ru/profile/vlad.shakhuro/
GoogleScholarPage: https://scholar.google.com/citations?user=MuPLmJsAAAAJ
Photo: images/people/vlad_shakhuro.jpg
ResearchInterests: object recognition in images, generating synthetic training data for computer vision algorithms, generative neural networks
Projects: traffic_sign_recognition, traffic_sign_recognition_with_synthetic_datasets

Vlad Shakhuro is a junior researcher in Graphics & Media Lab, Faculty of Computational Mathematics and Cybernetics, Lomonosov Moscow State University. He is also a lecturer in Computer vision course in MSU, Yandex school for data analysis and National Research University Higher School of Economics.

He received his PhD in Computer Science from National Research University Higher School of Economics in 2021.

**PhD Thesis**: Rare traffic sign recognition using synthetic training data
