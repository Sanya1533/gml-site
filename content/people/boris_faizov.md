Active: no
Category: people
PersonType: masters
Id: boris_faizov
Name: Boris
Surname: Faizov
PublicationsPossibleNames: Борис
PublicationsPossibleSurnames: Фаизов
ParseNewPublications: yes
LabGroups: cv_group
PersonOrder: 0
Position: Master's student
IstinaPage: https://istina.msu.ru/profile/faizov_boris/
Photo: images/people/boris_faizov.jpg
ResearchInterests: Computer vision, Machine Learning
Projects: traffic_sign_recognition, traffic_sign_recognition_with_synthetic_datasets
SelectedPublications: faizov20klassifikatsija, shakhuro19traffic

I am Master's student
