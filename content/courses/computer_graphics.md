Active: yes
Category: courses
Id: computer_graphics
Title: Computer graphics
Image: images/missing_image.jpg
Teachers: vladimir_frolov
CourseOrder: 10

Course program include human optical system, color and light modeling, basic image processing and analisis, basics of signal processing, rendering pipeline, OpenGL, introduction to shaders, global illumination, ray tracing and radiosity methods.
