Active: yes
Category: courses
Id: computer_vision
Title: Computer vision
Image: images/missing_image_white.jpg
Teachers: anton_konushin, vlad_shakhuro
CourseOrder: 0

Course is dedicated to 2D computer vision methods. Program includes image
formation process, image filtering, local features detection and matching,
image retrieval, robust fitting methods, introduction to machine learning and
deep learning, image categorization, object detection, image segmentation,
optical flow estimation, background subtraction, object tracking, action
recognition. Lectures are accompanied by seminars and practical tasks.The
course is taught in Moscow State University and Yandex School of Data Analysis.
