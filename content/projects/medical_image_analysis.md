Active: yes
Category: projects
ProjectType: ongoing
Id: medical_image_analysis
ProjectOrder: 85
SmallPhoto: images/projects/medical_image_analysis_small.png
Photo:
Title: Medical image analysis
ContactPerson: olga_senyukova
Team: olga_senyukova, sergey_pyatkovsky, gregory_shoroshov, sergey_trubetskoy, dmitry_vartanov
SelectedPublications: zheniy21discovery, gavrishchaka19synergy, senyukova19automated, senyukova15brain, senyukova14segmentation

The main goal of this research direction is to develop the instruments that help doctors in various data analysis tasks. It is important that we are talking not about the replacement of a doctor by artificial intelligence, but about

- reducing the amount of routine work, thus allowing to save more lives
- finding invisible or unobvious patterns in data for better diagnostics

and other kinds of assistance.

Medical imaging refers to visualization of internal structures of human body. Our projects focus on magnetic resonance images (MRI) and computed tomography (CT) images. Besides images, we work with other kinds of signals such as electrocardiogram (ECG) and gait data from clinical and portable devices. We develop deep learning-based algorithms for automatic analysis of this data. Currently our group has a joint work with
[Diagnostics and Telemedicine Center of the Moscow Health Care Department](https://tele-med.ai/) and also with
[research group from the USA](http://www.medpobs.com/).

Our projects include:

- human body composition analysis, involving segmentation of fat and muscle tissues
- image-based detection and prediction of MRI hardware failures
- diagnostics of heart and neurological diseases and monitoring of psychophysiological states.
