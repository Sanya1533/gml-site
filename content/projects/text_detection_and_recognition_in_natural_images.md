Active: yes
Category: projects
ProjectType: completed
Id: text_detection_and_recognition_in_natural_images
ProjectOrder: 100
SmallPhoto: images/projects/text_detection_and_recognition_in_natural_images.png
Photo: images/projects/text_detection_and_recognition_in_natural_images.png
Title: Text detection and recognition in natural images
ContactPerson: olga_barinova
Team: olga_barinova, anton_konushin
SelectedPublications:

Automatic detection and understanding the text in natural images, such as photographs of city outdoors or building indoors, is a challenging problem. There is a considerable gap between detecting and understanding text in scanned documents (which is a mature technology) and detecting and understanding text in the natural images. Detection and understanding the text in natural photographs involves localizing the text as well as removing the variation factors, such as varying text orientation, font, color and lighting. Examples of natural scene texts are shown below.

# Acknowledgements

This project is supported by Microsoft Research programs in Russia.
