Active: yes
Title: Поздравляем студентку лаборатории Елену Третьяк
Category: news
Date: 2010-05-17 00:00

Поздравляем Елену Третьяк с присуждением именной стипендии "The Google Anita Borg Memorial Scholarship"!
Стипендия имени Аниты Борг присуждется девушкам, занимающимся computer science и учащихся на "отлично".
[Подробнее...](http://googleblog.blogspot.com/2010/05/introducing-googles-2010-anita-borg.html)
