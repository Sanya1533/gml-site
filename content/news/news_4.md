Active: yes
Title: Defense of Ph.D. thesis of Dmitry Kulikov
Category: news
Date: 2009-05-12 00:00

Defense of Ph.D. thesis of Dmitry Kulikov will take place on May 15th, 2009, at 11 a.m. in a room 685 of MSU CMC (VMiK) dept., bldg. 2 (you can [download thesis in russian](http://cs.msu.ru/science/1.4/1.4.2.2/200904-kdl.pdf))
