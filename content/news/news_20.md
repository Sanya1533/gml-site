Active: yes
Title: Результаты собеседования в лабораторию 2010
Category: news
Date: 2010-04-23 00:00

Список людей, которые получат рекомендацию в лабораторию компьютерной графики и мультимедиа по результатам собеседования.
Информация о времени и месте получения рекомендации будет доступна позднее.

1. Артюхин Станислав Геннадьевич
2. Батанов Павел Георгиевич
3. Бобрик Ксения Петровна
4. Гурьянов Антов Константинович
5. Ерофеев Михаил Викторович
6. Зачесов Антон Александрович
7. Карпухин Иван Александрович
8. Конев Артем Александрович
9. Костин Григорий Александрович
10. Меркулов Кирилл Дмитриевич
11. Меркулова Татьяна Дмитриевна
12. Новикова Татьяна Владимировна
13. Новоторцев Леонид Владимирович
14. Птенцов Сергей Владимирович
15. Самсонов Павел Адреевич
16. Сопатов Александр Сергеевич
17. Сторожилова Мария Вадимовна
18. Сумин Денис Александрович
19. Тимонин Павел Михайлович
20. Шальнов Евгений Вадимович
