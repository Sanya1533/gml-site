Active: yes
Title: Mikhail Sindeyev defended his PhD thesis
Category: news
Date: 2013-05-21 00:00

Mikhail Sindeyev defended his PhD thesis “Research and development of video matting algorithms” on May 21, 2013 in [Keldysh Institute of Applied Mathematics RAS](http://keldysh.ru/council/1/).

Our congratulations to Mikhail!
