Active: yes
Title: GML C++ Camera Calibration Toolbox en
Category: news
Date: 2011-12-23 00:00

A new version of “GML C++ Camera Calibration Toolbox” has been released. Now the toolbox supports multiple calibration patterns within one project. During calibration a relative orientation between pattern coordinate systems is recovered. The usage of multiple patterns makes the calibration procedure more stable. This improves calibration accuracy. Also, less images for calibration is needed..
