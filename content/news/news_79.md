Active: yes
Title: We congratulate the graduates of our lab, who won prizes on best thesis contest in CMC MSU!
Category: news
Date: 2013-06-25 00:00

We congratulate the graduates of our lab, who won prizes on best thesis contest in CMC MSU!

Our winners:

- Tatyana Novikova (advisor – Olga Barinova) – 1st prize
- Eugeniy Shalnov (advisor – Anton Konushin) – 3rd prize
- Sergey Ushakov (advisor – Anton Konushin) – 3rd prize
